﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly MailService _mailService;

        public PostsController(PostService postService, LikeService likeService, MailService mailService)
        {
            _postService = postService;
            _likeService = likeService;
            _mailService = mailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpGet("get/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> GetPost(int id)
        {
            return Ok(await _postService.GetPost(id));
        }

        [HttpPost("notify")]
        [AllowAnonymous]
        public async Task<IActionResult> NotifyAboutLike([FromBody]PostDTO post)
        {
            await _mailService.SendLikeNotification(post);
            return Ok();
        }


        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }


        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await _postService.DeletePost(id);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdatePost([FromBody] PostUpdateDTO post)
        {
            await _postService.UpdatePost(post);
            return Ok(post);
        }
    }
}