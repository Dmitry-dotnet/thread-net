﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class likeServiceLogicChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDislike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsDislike",
                table: "CommentReactions");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 14, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(317), false, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(320), 3 },
                    { 2, 17, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9726), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9742), 3 },
                    { 3, 12, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9782), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9785), 18 },
                    { 4, 5, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9810), true, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9813), 16 },
                    { 5, 1, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9858), true, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9862), 2 },
                    { 6, 10, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9888), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9891), 12 },
                    { 7, 19, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9914), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9918), 19 },
                    { 8, 2, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9941), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9945), 20 },
                    { 9, 19, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9966), false, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9969), 10 },
                    { 10, 7, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9992), true, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9995), 11 },
                    { 1, 17, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(8016), true, new DateTime(2020, 6, 19, 12, 50, 13, 48, DateTimeKind.Local).AddTicks(9074), 9 },
                    { 12, 17, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(110), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(114), 10 },
                    { 13, 9, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(141), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(144), 21 },
                    { 14, 18, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(166), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(169), 20 },
                    { 15, 17, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(190), false, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(194), 18 },
                    { 16, 19, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(217), false, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(220), 4 },
                    { 17, 1, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(242), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(245), 16 },
                    { 18, 2, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(266), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(269), 3 },
                    { 19, 10, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(291), true, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(294), 8 },
                    { 11, 8, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(18), false, new DateTime(2020, 6, 19, 12, 50, 13, 49, DateTimeKind.Local).AddTicks(21), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Omnis repudiandae id blanditiis et voluptatum.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(1872), 3, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(3030) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Labore rerum quia.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(3962), 8, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(3979) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Incidunt alias officia non et.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4078), 6, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4082) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Rerum assumenda minus et perferendis architecto.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4159), 9, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4163) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Consequatur facilis qui quia dicta harum voluptates sit et.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4273), 7, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4276) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Excepturi voluptas sunt modi possimus facere.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4347), 14, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4351) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Voluptates debitis sint provident pariatur nobis rerum similique possimus.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4434), 18, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4438) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Necessitatibus ipsam velit ut beatae ratione consequatur cum doloremque.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4577), 7, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4582) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Eveniet autem dignissimos rerum nobis quaerat quia atque placeat saepe.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4670), 16, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4674) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Rerum ut hic accusantium laborum.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4737), 5, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4740) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Est dolorum perferendis necessitatibus nobis cum.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4799), 8, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4803) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Eum quia sed.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4853), 13, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4857) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Et et rerum et quo voluptate.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4922), 20, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(4925) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Et ut iusto repudiandae quam et.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5038), 6, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5042) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Molestias non facere.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5092), 16, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5096) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Culpa quisquam consequuntur.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5146), 2, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5150) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Incidunt qui consequatur possimus nihil dolor qui.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5215), 11, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5219) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Facilis dolorum animi.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5264), 9, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5268) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Eligendi recusandae commodi esse.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5323), 9, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5327) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Molestiae quisquam vel.", new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5375), 18, new DateTime(2020, 6, 19, 12, 50, 13, 10, DateTimeKind.Local).AddTicks(5379) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 382, DateTimeKind.Local).AddTicks(9359), "https://s3.amazonaws.com/uifaces/faces/twitter/S0ufi4n3/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(6384) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7418), "https://s3.amazonaws.com/uifaces/faces/twitter/simobenso/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7448) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7490), "https://s3.amazonaws.com/uifaces/faces/twitter/karsh/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7495) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7522), "https://s3.amazonaws.com/uifaces/faces/twitter/sdidonato/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7526) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7550), "https://s3.amazonaws.com/uifaces/faces/twitter/aleksitappura/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7554) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7576), "https://s3.amazonaws.com/uifaces/faces/twitter/jordyvdboom/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7580) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7603), "https://s3.amazonaws.com/uifaces/faces/twitter/liminha/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7607) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7631), "https://s3.amazonaws.com/uifaces/faces/twitter/thehacker/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7634) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7658), "https://s3.amazonaws.com/uifaces/faces/twitter/kimcool/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7684), "https://s3.amazonaws.com/uifaces/faces/twitter/nicklacke/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7688) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7710), "https://s3.amazonaws.com/uifaces/faces/twitter/vaughanmoffitt/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7713) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7736), "https://s3.amazonaws.com/uifaces/faces/twitter/adobi/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7739) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7762), "https://s3.amazonaws.com/uifaces/faces/twitter/cocolero/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7766) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7788), "https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7792) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7813), "https://s3.amazonaws.com/uifaces/faces/twitter/derekcramer/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7816) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7840), "https://s3.amazonaws.com/uifaces/faces/twitter/AlbertoCococi/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7843) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(7996), "https://s3.amazonaws.com/uifaces/faces/twitter/noxdzine/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8029), "https://s3.amazonaws.com/uifaces/faces/twitter/jitachi/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8032) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8055), "https://s3.amazonaws.com/uifaces/faces/twitter/kurtinc/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8081), "https://s3.amazonaws.com/uifaces/faces/twitter/kojourin/128.jpg", new DateTime(2020, 6, 19, 12, 50, 12, 383, DateTimeKind.Local).AddTicks(8085) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(4582), "https://picsum.photos/640/480/?image=31", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(5978) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6484), "https://picsum.photos/640/480/?image=353", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6533) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6574), "https://picsum.photos/640/480/?image=203", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6578) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6603), "https://picsum.photos/640/480/?image=998", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6606) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6627), "https://picsum.photos/640/480/?image=588", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6651), "https://picsum.photos/640/480/?image=105", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6654) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6674), "https://picsum.photos/640/480/?image=457", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6678) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6910), "https://picsum.photos/640/480/?image=649", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6915) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6940), "https://picsum.photos/640/480/?image=986", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6943) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6964), "https://picsum.photos/640/480/?image=406", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6967) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6987), "https://picsum.photos/640/480/?image=794", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(6991) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7011), "https://picsum.photos/640/480/?image=842", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7015) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7035), "https://picsum.photos/640/480/?image=770", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7039) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7059), "https://picsum.photos/640/480/?image=307", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7062) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7083), "https://picsum.photos/640/480/?image=56", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7086) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7107), "https://picsum.photos/640/480/?image=842", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7110) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7131), "https://picsum.photos/640/480/?image=856", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7154), "https://picsum.photos/640/480/?image=838", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7157) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7178), "https://picsum.photos/640/480/?image=1039", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7181) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7200), "https://picsum.photos/640/480/?image=912", new DateTime(2020, 6, 19, 12, 50, 12, 408, DateTimeKind.Local).AddTicks(7204) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5986), false, 12, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5989), 14 },
                    { 11, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6015), true, 6, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6018), 19 },
                    { 12, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6043), false, 11, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6046), 19 },
                    { 13, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6144), false, 14, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6148), 7 },
                    { 15, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6207), false, 5, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6211), 12 },
                    { 18, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6485), false, 5, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6489), 21 },
                    { 16, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6417), true, 12, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6424), 16 },
                    { 17, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6458), false, 18, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6462), 13 },
                    { 9, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5957), true, 11, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5961), 14 },
                    { 20, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6541), true, 6, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6544), 20 },
                    { 19, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6513), true, 20, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6517), 13 },
                    { 14, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6178), false, 19, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(6182), 6 },
                    { 8, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5926), true, 4, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5930), 1 },
                    { 3, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5770), true, 9, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5774), 11 },
                    { 6, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5865), true, 3, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5868), 12 },
                    { 5, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5836), false, 12, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5839), 10 },
                    { 1, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(3745), false, 6, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(4946), 21 },
                    { 4, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5805), true, 18, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5809), 15 },
                    { 2, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5705), true, 17, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5724), 20 },
                    { 7, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5893), false, 12, new DateTime(2020, 6, 19, 12, 50, 13, 32, DateTimeKind.Local).AddTicks(5896), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Autem iure tempore id cum et ad ipsam architecto repudiandae.", new DateTime(2020, 6, 19, 12, 50, 12, 988, DateTimeKind.Local).AddTicks(3046), 21, new DateTime(2020, 6, 19, 12, 50, 12, 988, DateTimeKind.Local).AddTicks(4302) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "quod", new DateTime(2020, 6, 19, 12, 50, 12, 988, DateTimeKind.Local).AddTicks(6111), 39, new DateTime(2020, 6, 19, 12, 50, 12, 988, DateTimeKind.Local).AddTicks(6141) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Earum doloremque rerum corporis. Corrupti id et rem repellat qui dignissimos laudantium est beatae. Reprehenderit voluptate voluptatem dolore voluptatem fugiat mollitia ratione voluptate.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(2515), 33, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(2535) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Quia minima est. Blanditiis et corrupti vitae placeat sed architecto qui. Voluptatem nisi totam culpa dolorum aperiam sint perspiciatis sint. Aut sed odit et quod eveniet minima et distinctio cupiditate. Tempore qui facere omnis ut numquam.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(3006), 27, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(3013) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Velit doloremque minima et.
Veritatis numquam et quibusdam molestias voluptate suscipit rerum similique ut.
Placeat sit totam totam velit ullam.
Est saepe porro aut iure qui.
Ut quaerat ipsa harum sint beatae sequi.
Sapiente numquam iusto facere est repudiandae voluptatem fugit accusamus quia.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4569), 38, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4587) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Iusto omnis ut facilis.
Voluptates modi quaerat eum dicta numquam ad voluptatem.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4788), 26, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4794) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Consequatur modi aliquam doloremque distinctio.
Voluptatum eum omnis nulla mollitia quas ipsa.
Odit et id.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4969), 38, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(4974) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Doloremque quod tenetur est libero excepturi quidem.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5055), 23, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5059) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Et est et itaque voluptate aut voluptas.
Dolore magni autem nam.
Ipsa numquam dolorem vitae est.
Harum fugiat facere dolor deleniti ullam.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5349), 29, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5354) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "quo", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5391), 26, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5395) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "eaque", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5430), 38, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5434) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "sed", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5469), 23, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5472) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Minima eveniet commodi nostrum veniam. Ullam rem doloremque dolores ut non dolor. Doloribus nihil rerum maiores odit enim sit reiciendis. Ratione fuga iusto quas. Sapiente tenetur quos laborum earum similique voluptatum rerum sed consequatur.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5747), 23, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(5752) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Illum doloribus aut odit. Ut dolor quos non distinctio quam recusandae corporis porro. Libero pariatur sed enim. Beatae facilis fuga maiores numquam enim odit.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6043), 27, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6048) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Delectus id ea sequi sint voluptas impedit. Sit laudantium minima hic cupiditate harum quia dolorem eveniet. Nostrum dolore ducimus vero neque. Consequatur quia saepe cupiditate. Voluptatum vero commodi et.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6350), 39, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6356) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Voluptatem occaecati velit.
Quia laborum consequatur cum laudantium culpa explicabo.
Voluptate exercitationem natus quos veritatis aut in amet temporibus qui.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6510), 29, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6515) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Repudiandae accusantium laborum nam est consequatur quo.
Assumenda quo necessitatibus provident.
Consequatur est distinctio enim molestiae consectetur quasi.
Aut et pariatur vero doloribus quia ad.
Dicta modi recusandae et consequatur qui est pariatur sint.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6779), 30, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6784) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Et quia in doloremque vel aspernatur id quia omnis.
Quidem ab odit mollitia omnis aut.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6897), 33, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(6901) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, @"Et tenetur repudiandae ut ex eaque consequatur qui.
Inventore et qui sit omnis cum minima sapiente quibusdam quia.
Delectus laudantium et.
A nisi illo dicta fugit accusamus dolores consequatur tempore aut.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(7224), 21, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(7230) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Veritatis molestiae est.
Autem eos mollitia sunt qui dicta cupiditate expedita voluptas.
Esse quisquam sequi qui recusandae qui.
Maiores quisquam veniam.", new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(7444), 22, new DateTime(2020, 6, 19, 12, 50, 12, 989, DateTimeKind.Local).AddTicks(7448) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 19, 12, 50, 12, 507, DateTimeKind.Local).AddTicks(6322), "Delbert.Mills33@gmail.com", "2xLs33sGyIRIwcRpGQN3ZfhZ+UVBCGSQ/Z58n+bizLQ=", "qsBA15s//cXoWdNNCSEI6AEp6mnC1Jq4vwhJagUnAQA=", new DateTime(2020, 6, 19, 12, 50, 12, 507, DateTimeKind.Local).AddTicks(7828), "Jackie.Wolff" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 529, DateTimeKind.Local).AddTicks(318), "Milford22@hotmail.com", "qVWm2d8Zg+bdAZ0jyVKb5IUsYfr6jbp6p/3pm3tfQWo=", "ewUERCSNue1AZNYDrswTTq0KWPLnlKqchT8QyquDkiw=", new DateTime(2020, 6, 19, 12, 50, 12, 529, DateTimeKind.Local).AddTicks(398), "Sonya.Lebsack" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 548, DateTimeKind.Local).AddTicks(6966), "Eugenia_Hickle45@yahoo.com", "MPX6o5lXERJeN4ifv3KndfQTaRaSDZJdWvKd62xZAAM=", "aVKYS2p6xpWL9PnXE7jqB1OK2GHtz4fNlnDE5bC75A0=", new DateTime(2020, 6, 19, 12, 50, 12, 548, DateTimeKind.Local).AddTicks(7124), "Aryanna65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 12, 50, 12, 567, DateTimeKind.Local).AddTicks(2836), "Anais.Hand@gmail.com", "2ZuJdZYEQkzMBoVmhP/NAK9Sb/EFoGYmxdn6dtSxAO8=", "IT5+oP9JcajhgutNbGTjQ9LRERW6vl8iv5N5tOcKhjk=", new DateTime(2020, 6, 19, 12, 50, 12, 567, DateTimeKind.Local).AddTicks(2889), "Llewellyn_Howell6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 12, 50, 12, 591, DateTimeKind.Local).AddTicks(853), "Krista.Murazik91@hotmail.com", "gxLq/30HVQkyVburmSgvOkGUD4rqV6jYDTWymdq6CqM=", "yV9cQjfAnoEsm7nUxsOnOTe5hC9lL+xEtCVt74EWOh0=", new DateTime(2020, 6, 19, 12, 50, 12, 591, DateTimeKind.Local).AddTicks(908), "Kiley6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 19, 12, 50, 12, 611, DateTimeKind.Local).AddTicks(6443), "Clifton_Yundt@yahoo.com", "FsY2kU3OshefZNOTdmgmP3w8DkczbAM46z4wlDLViB8=", "XQz1rI34GAvf8aatyUeVSBpT6Z6B4IADlo6n5Jt46aQ=", new DateTime(2020, 6, 19, 12, 50, 12, 611, DateTimeKind.Local).AddTicks(6506), "Jaydon.Dicki" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 12, 50, 12, 632, DateTimeKind.Local).AddTicks(5611), "Justice50@hotmail.com", "pyzz8JTvLhNAUnQZ2gZ08NCgmh+Krf8aSlqZWv6GNiA=", "g/dy/tWmPdS5AHnGplIzv8p+5prUjIdq2tr459aohB4=", new DateTime(2020, 6, 19, 12, 50, 12, 632, DateTimeKind.Local).AddTicks(5664), "Jeanette_Breitenberg15" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 12, 50, 12, 656, DateTimeKind.Local).AddTicks(9427), "Nella_Altenwerth71@gmail.com", "uE3pJzBESgpw3A0clxccCrpnuL/h+TeLsvSeJygM04s=", "UMY+wsiT7yh71BfLSMkMHBK4I/B6PVc94pPfhIn1fIA=", new DateTime(2020, 6, 19, 12, 50, 12, 656, DateTimeKind.Local).AddTicks(9482), "Leta_Abbott50" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 12, 50, 12, 677, DateTimeKind.Local).AddTicks(7098), "Logan_Rutherford@yahoo.com", "ikEKje1uvwlSQJ/3BszKBIOw895CFJhIdiWUdISv4GM=", "gVkyJv4/Q6T1Kzkas6f+jcBbwUJMDu7M9deExpnVulc=", new DateTime(2020, 6, 19, 12, 50, 12, 677, DateTimeKind.Local).AddTicks(7264), "Paige95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 12, 50, 12, 698, DateTimeKind.Local).AddTicks(7792), "Winnifred18@hotmail.com", "gvkqggJrDTqQmNvOjJQjg350TNyh1hTMYglRkw8ZxL4=", "+i6h6phqz+jydM1LDzDy4gJS1jfe31rWA8SAdwaFDww=", new DateTime(2020, 6, 19, 12, 50, 12, 698, DateTimeKind.Local).AddTicks(7944), "Tom_Rolfson" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 19, 12, 50, 12, 724, DateTimeKind.Local).AddTicks(9835), "Madge71@hotmail.com", "sPr3ZOLoVfaubZqe59ljpGLtMNjfvOSSH2QIKTugibM=", "K9LF/w8bAjI7HoH+FDRgUZAWaodJtG497Qh371qHlvI=", new DateTime(2020, 6, 19, 12, 50, 12, 725, DateTimeKind.Local).AddTicks(30), "Angela_Hilll11" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 12, 50, 12, 746, DateTimeKind.Local).AddTicks(1003), "Eldora.Hayes@yahoo.com", "4OBl/of2QeiOorOaz/gUEJZLRjIs2RjQYCCkW6daTzg=", "0IEyYv0C4fLaYDeEo1hjhS24X+tqF8vz1t9b93VCW6M=", new DateTime(2020, 6, 19, 12, 50, 12, 746, DateTimeKind.Local).AddTicks(1061), "Cloyd43" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 12, 50, 12, 771, DateTimeKind.Local).AddTicks(5054), "Edwina_Cremin@gmail.com", "76PvsTSNSfmZp1RrPKmhltnmJG6W5afp2Tf1Ko/Wy/k=", "RzO49hWEYSVN5bUETKT5ViFcAh4sHraNPXcmODtuCYc=", new DateTime(2020, 6, 19, 12, 50, 12, 771, DateTimeKind.Local).AddTicks(5113), "Danny.Klocko" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 19, 12, 50, 12, 792, DateTimeKind.Local).AddTicks(2920), "Mathew54@hotmail.com", "Mmci2GmnXhqCFYp6NlfkwL+Ff6bK49cHmEfuPDf9blM=", "PqyIuTzBfo6M8bfqKl96fWp3Pjr8uic/tpJfipIhPdY=", new DateTime(2020, 6, 19, 12, 50, 12, 792, DateTimeKind.Local).AddTicks(2968), "Elenor_Nikolaus10" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 19, 12, 50, 12, 813, DateTimeKind.Local).AddTicks(2096), "Dorothy.Waelchi@gmail.com", "4t92G1JVuptj1AWlfdksHgY5BAX9BDDBKB13dDdoGF4=", "HKXuoXJweEyG9ULLg2tSViEAEZ4gHadChiP8tybzTC0=", new DateTime(2020, 6, 19, 12, 50, 12, 813, DateTimeKind.Local).AddTicks(2167), "Gerda70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 12, 50, 12, 839, DateTimeKind.Local).AddTicks(9017), "Rahul_Olson@yahoo.com", "FLnIgr20KGC59WoCeeelOed6wSUXTB0xS2Yw+mBMz2k=", "Qeob+hblN6WwK3M47OJBGDYISpCuT2xVwOo/RUNR2Bc=", new DateTime(2020, 6, 19, 12, 50, 12, 839, DateTimeKind.Local).AddTicks(9075), "Charlie73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 19, 12, 50, 12, 861, DateTimeKind.Local).AddTicks(2933), "Hazel.Rempel@gmail.com", "GkZ5i22/UiBPM+KPd6PU1gLSoD86C2bSmrF2vedO0EM=", "hpWy9hXiNn26p/tQ9IUi2hO00A61mHm60gwcW270rsM=", new DateTime(2020, 6, 19, 12, 50, 12, 861, DateTimeKind.Local).AddTicks(2980), "Dustin_Jacobi53" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 19, 12, 50, 12, 882, DateTimeKind.Local).AddTicks(263), "Stuart.Schuppe@gmail.com", "RCXU1iXOgfF9XL0tTL/eEO0fyGGlZ6I8FdV9yGFas8E=", "rNwtWR20UhEL2G0ioCI4C/6vw6UVidiigvknUE533+I=", new DateTime(2020, 6, 19, 12, 50, 12, 882, DateTimeKind.Local).AddTicks(321), "Litzy.Lang23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 19, 12, 50, 12, 909, DateTimeKind.Local).AddTicks(406), "Alexandro40@yahoo.com", "AfjbYuca0RSKMMbiXMLQi3DOKz8777IfplArQh9iMY4=", "+XGvJeC6QMbORqx0pivuUpLrTrOHXoDfHD5WRvXgC4Q=", new DateTime(2020, 6, 19, 12, 50, 12, 909, DateTimeKind.Local).AddTicks(465), "Destiney_Stehr27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 12, 50, 12, 930, DateTimeKind.Local).AddTicks(1494), "Bernard_Mueller@hotmail.com", "GsvxmznG3iP+j7Kyr/o5RKTvGSLmB8FdA/bOUdyRu2k=", "EC89MVCAM+ixCaInjoMugig0teJ0fREMdmVK3debFd4=", new DateTime(2020, 6, 19, 12, 50, 12, 930, DateTimeKind.Local).AddTicks(1636), "Laisha5" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 12, 50, 12, 951, DateTimeKind.Local).AddTicks(2679), "Vxsuyfu0274idhoV+wtxgJITR9qySMeXVNgxlPCsGck=", "NlLCJd4RgJEJJR+rAu5SnvJVDDUX5hmyF9HKcclyKF0=", new DateTime(2020, 6, 19, 12, 50, 12, 951, DateTimeKind.Local).AddTicks(2679) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDislike",
                table: "PostReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDislike",
                table: "CommentReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsDislike", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 16, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(677), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(681), 15 },
                    { 2, 6, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9557), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9576), 20 },
                    { 3, 17, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9621), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9625), 12 },
                    { 4, 4, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9654), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9658), 3 },
                    { 5, 4, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9684), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9687), 14 },
                    { 6, 8, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9714), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9717), 5 },
                    { 7, 13, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9743), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9747), 4 },
                    { 8, 12, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9773), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9776), 9 },
                    { 9, 6, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9800), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9803), 4 },
                    { 10, 1, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9857), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9860), 18 },
                    { 1, 5, new DateTime(2020, 6, 8, 14, 2, 51, 635, DateTimeKind.Local).AddTicks(5679), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(8270), 12 },
                    { 12, 5, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(420), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(429), 6 },
                    { 13, 10, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(477), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(481), 17 },
                    { 14, 6, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(507), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(510), 7 },
                    { 15, 9, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(535), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(538), 5 },
                    { 16, 19, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(564), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(568), 2 },
                    { 17, 19, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(593), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(597), 16 },
                    { 18, 20, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(621), false, false, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(625), 13 },
                    { 19, 3, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(649), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 640, DateTimeKind.Local).AddTicks(653), 18 },
                    { 11, 20, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9887), false, true, new DateTime(2020, 6, 8, 14, 2, 51, 639, DateTimeKind.Local).AddTicks(9890), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Et quos vel.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(3981), 5, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(5987) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Quo eligendi iusto neque odio quis explicabo quidem inventore.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7194), 2, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7219) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Ullam quia cum.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7356), 4, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7360) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Incidunt hic consequatur ab recusandae numquam facilis enim voluptatem ullam.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7462), 2, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7466) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Beatae quia minus placeat laborum saepe voluptatibus labore aut.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7551), 18, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7554) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Maiores sit possimus nulla aspernatur eum.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7625), 7, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7628) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Sed aliquam omnis distinctio omnis eius.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7769), 3, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7773) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Quibusdam ipsam reiciendis totam.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7831), 20, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7835) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quis et et sit facilis aliquid eaque.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7903), 12, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7906) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Adipisci rerum totam ab repudiandae ut facere ea a accusamus.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7995), 2, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(7999) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Tempore laudantium fugit expedita fugit sed dolores sint accusantium.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8079), 6, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8082) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ipsam et qui at voluptas numquam id dicta.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8250), 1, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8255) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Distinctio quia aut beatae modi aut aperiam explicabo.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8336), 6, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8339) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Reiciendis porro non optio.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8395), 13, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8398) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Et ipsum explicabo.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8445), 7, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8449) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Et laborum molestias consectetur expedita quis alias.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8518), 13, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8521) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Qui deleniti ut blanditiis ad voluptate asperiores optio est.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8599), 3, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8603) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Nihil quia quasi nostrum voluptatibus alias.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8760), 18, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8765) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Voluptatum at veniam ad laborum id voluptas.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8842), 5, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8845) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quia sit rerum architecto numquam.", new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8908), 4, new DateTime(2020, 6, 8, 14, 2, 51, 598, DateTimeKind.Local).AddTicks(8911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 981, DateTimeKind.Local).AddTicks(3305), "https://s3.amazonaws.com/uifaces/faces/twitter/okandungel/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(4029) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6164), "https://s3.amazonaws.com/uifaces/faces/twitter/smaczny/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6280), "https://s3.amazonaws.com/uifaces/faces/twitter/kikillo/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6284) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6311), "https://s3.amazonaws.com/uifaces/faces/twitter/stan/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6339), "https://s3.amazonaws.com/uifaces/faces/twitter/suribbles/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6342) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6365), "https://s3.amazonaws.com/uifaces/faces/twitter/shojberg/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6368) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6475), "https://s3.amazonaws.com/uifaces/faces/twitter/shesgared/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6478) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6502), "https://s3.amazonaws.com/uifaces/faces/twitter/ahmadajmi/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6505) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6528), "https://s3.amazonaws.com/uifaces/faces/twitter/davecraige/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6551), "https://s3.amazonaws.com/uifaces/faces/twitter/motionthinks/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6554) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6579), "https://s3.amazonaws.com/uifaces/faces/twitter/richwild/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6582) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6606), "https://s3.amazonaws.com/uifaces/faces/twitter/jennyyo/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6608) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6630), "https://s3.amazonaws.com/uifaces/faces/twitter/kaysix_dizzy/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6714), "https://s3.amazonaws.com/uifaces/faces/twitter/cynthiasavard/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(6719) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7136), "https://s3.amazonaws.com/uifaces/faces/twitter/oscarowusu/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7140) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7163), "https://s3.amazonaws.com/uifaces/faces/twitter/kirangopal/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7166) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7188), "https://s3.amazonaws.com/uifaces/faces/twitter/rawdiggie/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7191) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7217), "https://s3.amazonaws.com/uifaces/faces/twitter/jlsolerdeltoro/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7221) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7568), "https://s3.amazonaws.com/uifaces/faces/twitter/danthms/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7573) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7602), "https://s3.amazonaws.com/uifaces/faces/twitter/artem_kostenko/128.jpg", new DateTime(2020, 6, 8, 14, 2, 50, 982, DateTimeKind.Local).AddTicks(7605) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(1166), "https://picsum.photos/640/480/?image=731", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2228) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2570), "https://picsum.photos/640/480/?image=281", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2606) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2634), "https://picsum.photos/640/480/?image=344", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2637) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2733), "https://picsum.photos/640/480/?image=392", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2737) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2759), "https://picsum.photos/640/480/?image=757", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2762) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2780), "https://picsum.photos/640/480/?image=457", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2783) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2802), "https://picsum.photos/640/480/?image=396", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2805) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2823), "https://picsum.photos/640/480/?image=625", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2826) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2844), "https://picsum.photos/640/480/?image=905", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2865), "https://picsum.photos/640/480/?image=608", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2868) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2886), "https://picsum.photos/640/480/?image=123", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2889) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2907), "https://picsum.photos/640/480/?image=273", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2929), "https://picsum.photos/640/480/?image=77", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2932) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2950), "https://picsum.photos/640/480/?image=457", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2953) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2971), "https://picsum.photos/640/480/?image=447", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2974) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2992), "https://picsum.photos/640/480/?image=215", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(2995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3059), "https://picsum.photos/640/480/?image=300", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3063) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3082), "https://picsum.photos/640/480/?image=931", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3085) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3103), "https://picsum.photos/640/480/?image=354", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3106) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3123), "https://picsum.photos/640/480/?image=674", new DateTime(2020, 6, 8, 14, 2, 50, 999, DateTimeKind.Local).AddTicks(3126) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsDislike", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9279), false, false, 11, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9282), 3 },
                    { 11, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9304), false, true, 12, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9307), 18 },
                    { 12, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9328), false, false, 8, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9331), 5 },
                    { 13, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9351), false, false, 20, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9354), 3 },
                    { 15, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9400), false, true, 15, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9403), 9 },
                    { 18, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9473), false, true, 15, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9475), 13 },
                    { 16, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9424), false, true, 6, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9427), 18 },
                    { 17, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9449), false, false, 12, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9452), 12 },
                    { 9, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9254), false, true, 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9257), 10 },
                    { 20, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9523), false, true, 1, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9526), 3 },
                    { 19, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9497), false, false, 5, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9500), 4 },
                    { 14, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9375), false, false, 19, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9378), 17 },
                    { 8, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9230), false, false, 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9233), 17 },
                    { 3, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9031), false, true, 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9034), 8 },
                    { 6, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9183), false, false, 20, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9186), 7 },
                    { 5, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9156), false, true, 14, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9160), 21 },
                    { 1, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(7377), false, false, 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(8365), 1 },
                    { 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9058), false, true, 8, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9127), 3 },
                    { 2, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(8982), false, true, 18, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(8996), 11 },
                    { 7, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9206), false, false, 4, new DateTime(2020, 6, 8, 14, 2, 51, 617, DateTimeKind.Local).AddTicks(9209), 20 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Nihil quo aperiam voluptate qui quia qui laborum autem facilis.", new DateTime(2020, 6, 8, 14, 2, 51, 576, DateTimeKind.Local).AddTicks(7123), 38, new DateTime(2020, 6, 8, 14, 2, 51, 576, DateTimeKind.Local).AddTicks(8718) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "quo", new DateTime(2020, 6, 8, 14, 2, 51, 577, DateTimeKind.Local).AddTicks(1212), 38, new DateTime(2020, 6, 8, 14, 2, 51, 577, DateTimeKind.Local).AddTicks(1240) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Aut quos repellendus sit eaque reiciendis voluptatem. Placeat rerum qui praesentium. Necessitatibus qui odit quis explicabo a voluptatem tempore. In autem aut molestias omnis voluptatem. Nihil possimus molestias dolores velit.", new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(6639), 26, new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(6658) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Et quis debitis reiciendis nam molestiae sit dolorem ea veniam.
Est odio voluptatibus animi quo et sunt asperiores.
Provident eius repellat.
Quae amet rerum quia ipsam est quis laudantium possimus aspernatur.", new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(9254), 31, new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(9272) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Accusamus non nisi ea id et explicabo voluptatem. Sed in et aliquid est praesentium laudantium. Autem voluptatibus recusandae voluptatum. Eos pariatur autem asperiores ut at nisi blanditiis hic ipsa.", new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(9980), 25, new DateTime(2020, 6, 8, 14, 2, 51, 581, DateTimeKind.Local).AddTicks(9990) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "qui", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(47), 24, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(50) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Tenetur accusamus quam temporibus quod.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(130), 40, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(133) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Ipsam ad consequatur totam quaerat rerum quasi quos. Maxime aliquid et aut quasi qui provident. Repellat cupiditate voluptatem minima aliquid fugit. Reiciendis est natus quasi aut voluptas inventore qui.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(420), 32, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(424) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "tempore", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(462), 25, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(465) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "labore", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(501), 22, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(505) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Molestiae quo eveniet ut quia quia non.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(587), 39, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(591) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Neque odit corrupti et quia repellat vel est nobis eveniet. Quaerat recusandae voluptates ullam tempore ut quis dolore. Inventore mollitia omnis ut impedit numquam et. Enim fugit neque qui ea quia excepturi magni ut dignissimos.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1415), 39, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1427) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Fugiat accusantium doloremque rerum esse exercitationem voluptates.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1587), 38, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1591) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "est", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1629), 36, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1633) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, @"Earum aut dolores voluptatum.
Similique id aspernatur eveniet omnis in.
Ea blanditiis autem quia.", new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1818), 26, new DateTime(2020, 6, 8, 14, 2, 51, 582, DateTimeKind.Local).AddTicks(1822) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Quasi et sunt asperiores. Architecto nesciunt vero deserunt ut rerum. Quae quas et earum quo nesciunt voluptatem nostrum recusandae. Expedita saepe quod qui officia et omnis et. Et culpa voluptatibus officiis enim natus.", new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2114), 30, new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2130) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Labore nemo quos blanditiis consequatur ea hic.", new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2305), 32, new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2310) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Quas praesentium nemo voluptas ratione.", new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2383), 34, new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2387) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "corrupti", new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2420), 31, new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2424) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "pariatur", new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2457), 23, new DateTime(2020, 6, 8, 14, 2, 51, 583, DateTimeKind.Local).AddTicks(2461) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 8, 14, 2, 51, 101, DateTimeKind.Local).AddTicks(6386), "Mable82@gmail.com", "oeFmfiHhuUR3WvlXgZNvCXBpfkCKHqlRuJV7GtYHLmE=", "NBipBlriZY71YA7QM6ai74Hge6t7rn+iuMMWC1OTjIU=", new DateTime(2020, 6, 8, 14, 2, 51, 101, DateTimeKind.Local).AddTicks(7718), "Ransom_OConnell" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 51, 127, DateTimeKind.Local).AddTicks(1802), "Alda21@yahoo.com", "KlqKjwEcwCGCCJheYVtIhsIUDNXVpRH4l5/BYtev9zU=", "4jRMGOLqwMkBm0mxeyZ4qcuKtxP00lOZHn4nq4s1DBI=", new DateTime(2020, 6, 8, 14, 2, 51, 127, DateTimeKind.Local).AddTicks(1832), "Leo.Lind" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 51, 148, DateTimeKind.Local).AddTicks(8424), "Dangelo.Quigley@yahoo.com", "5IDYPyYrldeWR25ZY7s7Qe7BS9KXl/epWJxuLVQR/KU=", "4U9i9NL/H7+gdMrBh3vyaeD1942jX4/TSXGSEvdwnRU=", new DateTime(2020, 6, 8, 14, 2, 51, 148, DateTimeKind.Local).AddTicks(8443), "Jett_Friesen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 8, 14, 2, 51, 169, DateTimeKind.Local).AddTicks(2276), "Willow_McCullough35@gmail.com", "BlNf9JPOAgKrRyXQLEegZUi446H+nfd7SJwWIDIjxi0=", "TZfOeRNdFTm5fGqmvxg+FzCWUBvbqUkSGMgzsGDSmWg=", new DateTime(2020, 6, 8, 14, 2, 51, 169, DateTimeKind.Local).AddTicks(2297), "Tevin.Gibson35" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 8, 14, 2, 51, 185, DateTimeKind.Local).AddTicks(1994), "Elmore_Kilback@gmail.com", "I87Done2L/Svb5SbwSvw97d13s4nL0O70oN3H1xVqjA=", "wwIG0lGrVfwOyGIOh0SkUVlhXtAUZnDLJI9qZPL0MtU=", new DateTime(2020, 6, 8, 14, 2, 51, 185, DateTimeKind.Local).AddTicks(2014), "Travon_Barrows" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 8, 14, 2, 51, 211, DateTimeKind.Local).AddTicks(3552), "Viviane.Murray@hotmail.com", "L79z794mtCzZxqal/91KrqlynsyEHAtmrUglX3qVd4I=", "/KWfnFcmidQSc+Y04yFIfWJ/NPFVQbclUQ5/lmxKMis=", new DateTime(2020, 6, 8, 14, 2, 51, 211, DateTimeKind.Local).AddTicks(3588), "Bridget75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 8, 14, 2, 51, 232, DateTimeKind.Local).AddTicks(2515), "Nya.Labadie13@gmail.com", "l2o0OGhGNqkcydLtO2zeK8QSphRDvU7HVi9QLSg+KHM=", "osqciRS/x1a85kh/8TnJ4FgcfEJAD6oLQb6xpGA203I=", new DateTime(2020, 6, 8, 14, 2, 51, 232, DateTimeKind.Local).AddTicks(2577), "Celia.Krajcik35" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 8, 14, 2, 51, 258, DateTimeKind.Local).AddTicks(3805), "Muriel_Raynor68@gmail.com", "kZZ+zFlkv7EEhPT3zc2+J4CGWjKgrCjwv9rSUXVMnck=", "W20ieedEaNbIgUnaDbqcF4s50a1gZ8lpYT1le87wMk4=", new DateTime(2020, 6, 8, 14, 2, 51, 258, DateTimeKind.Local).AddTicks(3969), "Victor_Schneider" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 8, 14, 2, 51, 279, DateTimeKind.Local).AddTicks(9190), "Stephen.Funk@gmail.com", "pgwZMY5EeiPn1X9aaqYQPd7Zh9VAR64fkxi89nYgVAE=", "IT3GPESZeDJyvou33aWrH6SdRaBLfDXivyet2qnYkgU=", new DateTime(2020, 6, 8, 14, 2, 51, 279, DateTimeKind.Local).AddTicks(9298), "Demario79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 8, 14, 2, 51, 308, DateTimeKind.Local).AddTicks(2491), "Jeramy_Dickens38@yahoo.com", "SZ4TK6cS1psdd2h2qMfIFbR24GTzCdOYsZvxO+f9nNc=", "nZIJucDSjzT6J2/Yz1bPXbFuOVvvJaIpgYSQiCy+iAI=", new DateTime(2020, 6, 8, 14, 2, 51, 308, DateTimeKind.Local).AddTicks(2518), "Rowan.Feil82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 8, 14, 2, 51, 332, DateTimeKind.Local).AddTicks(1714), "Samson_Fisher@hotmail.com", "z/a1JbQhtgqSVtoOfFW8iMiNktsLz4x+uqWEqNE9q7w=", "DOTUoYmhNnp7kY0udmunSxc6vPE6kojEhq1d6Po//N0=", new DateTime(2020, 6, 8, 14, 2, 51, 332, DateTimeKind.Local).AddTicks(1744), "Elsie.Casper11" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 8, 14, 2, 51, 353, DateTimeKind.Local).AddTicks(6909), "Junior_Blick33@gmail.com", "4cqvRkMd8JVKNDK6PMIp7/QIWByAQn3mZqppL0ga83Y=", "i3nZOyfTA5ctxxWNEcn8ks+Y7jm4p11z/e4/MBcWW7Y=", new DateTime(2020, 6, 8, 14, 2, 51, 353, DateTimeKind.Local).AddTicks(6938), "Jovanny84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 8, 14, 2, 51, 378, DateTimeKind.Local).AddTicks(1324), "Merlin13@hotmail.com", "f5b2sYVRPa4k3hkdW4/NLRikqzeFqNHTSPXXPVakiAw=", "HwCKjfjh75jVhnnTkNV06gcj/UWT1FfGXc4h5fMHx60=", new DateTime(2020, 6, 8, 14, 2, 51, 378, DateTimeKind.Local).AddTicks(1348), "Christ_Raynor5" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 8, 14, 2, 51, 401, DateTimeKind.Local).AddTicks(2800), "Lucas17@gmail.com", "9X0dtSWkr90QzTStZmcvk1+75Ib7t+S7GX4U87VrHhE=", "0JpCuU1BkewiwhW9UVQUPqKVjuhs1MtSzw4s8kW1hl8=", new DateTime(2020, 6, 8, 14, 2, 51, 401, DateTimeKind.Local).AddTicks(2822), "Katelin_Rosenbaum74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 8, 14, 2, 51, 423, DateTimeKind.Local).AddTicks(2158), "Maximillia7@hotmail.com", "wFweIcvsQuTumcvJM6Ji4wwpL49JL6K5lnJejujcYX0=", "o/XNrxxC+DT9Uo7extD0PbXhY4I4iqcZlwSmrj9ds00=", new DateTime(2020, 6, 8, 14, 2, 51, 423, DateTimeKind.Local).AddTicks(2182), "Beth_Schmeler" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 8, 14, 2, 51, 445, DateTimeKind.Local).AddTicks(325), "Dandre.Schneider54@gmail.com", "t/ADDftNi/I9jC7a4zouFx74JtBdPm/R2FuluGaN10Q=", "elL2ct2dQ4aIvzv+A5IA0VJ/YLcyA/yBJvF669ssltg=", new DateTime(2020, 6, 8, 14, 2, 51, 445, DateTimeKind.Local).AddTicks(454), "Donnell71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 8, 14, 2, 51, 461, DateTimeKind.Local).AddTicks(7025), "Santa_Reichel21@hotmail.com", "ZSW6FHqMiGYxGxkTiG1i7YHikaeNDelv5r4GUsfw5mU=", "qHjpqFij02Pth6DS5PeKJKBSgLMONebWkOiGDp/FuXA=", new DateTime(2020, 6, 8, 14, 2, 51, 461, DateTimeKind.Local).AddTicks(7044), "Reyes88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 8, 14, 2, 51, 486, DateTimeKind.Local).AddTicks(2873), "Romaine.Dickinson86@hotmail.com", "GGsK7qirn6S6p504CQhzrlVbu3zxnz5ocsbHCR4FHiQ=", "Fosex8Mi5LzuKhvVcwKpBf+csmKp95KHhXgBHjjV0E8=", new DateTime(2020, 6, 8, 14, 2, 51, 486, DateTimeKind.Local).AddTicks(2894), "Kellie_Hodkiewicz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 8, 14, 2, 51, 507, DateTimeKind.Local).AddTicks(8974), "Holly.Hoeger@yahoo.com", "gzHiUsvP/LGXFJfzuIR4l0Alb/L3PQQgmejM5bxDpZ0=", "rfWSblqSulTilv6s8DTYNnSjovAVWxEMV19vsbMJmgI=", new DateTime(2020, 6, 8, 14, 2, 51, 507, DateTimeKind.Local).AddTicks(8993), "Jennyfer_Wilkinson21" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 8, 14, 2, 51, 533, DateTimeKind.Local).AddTicks(4797), "Davon64@yahoo.com", "0ndBENS6dMwequVBQMQBqd5GRcQvIiM2Jfrp0w1EbTw=", "IEorRgW42Vhu+ARQ9P/m0sMMUhAMbNhQslSX+7mYHlU=", new DateTime(2020, 6, 8, 14, 2, 51, 533, DateTimeKind.Local).AddTicks(4816), "Ike42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 8, 14, 2, 51, 552, DateTimeKind.Local).AddTicks(3840), "dDaDUl5kW1iBgFVUAX0yIpqQbgJUrPXT9xF7CkGjYQ0=", "FVc3y6OYDDFtngfPdDLjEVeriC64TsQqF8MHY68D7yE=", new DateTime(2020, 6, 8, 14, 2, 51, 552, DateTimeKind.Local).AddTicks(3840) });
        }
    }
}
