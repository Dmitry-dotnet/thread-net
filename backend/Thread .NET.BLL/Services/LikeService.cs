﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reactionDto)
        {
            var like = _context.PostReactions.FirstOrDefault(x => x.UserId == reactionDto.UserId
                                                       && x.PostId == reactionDto.EntityId);

            if (like != null && like.IsLike == reactionDto.IsLike)
            {
                _context.PostReactions.RemoveRange(like);
                await _context.SaveChangesAsync();
                return;
            }
            if(like != null)
            {
                like.IsLike = reactionDto.IsLike;
                _context.PostReactions.Update(like);
            }
            else
            {
                _context.PostReactions.Add(new PostReaction()
                {
                    PostId = reactionDto.EntityId,
                    IsLike = reactionDto.IsLike,
                    UserId = reactionDto.UserId
                });
            }

            await _context.SaveChangesAsync();
        }

        public async Task LikeComment(NewReactionDTO reactionDto)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reactionDto.UserId
                                                       && x.CommentId == reactionDto.EntityId);
            if (likes.Any(x => x.IsLike == reactionDto.IsLike))
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
                return;
            }
            if (likes.Any())
            {
                //update logic
                var reaction = _mapper.Map<CommentReaction>(reactionDto);
                _context.CommentReactions.Update(reaction);
            }
            else
            {
                _context.CommentReactions.Add(new CommentReaction()
                {
                    CommentId = reactionDto.EntityId,
                    IsLike = reactionDto.IsLike,
                    UserId = reactionDto.UserId
                });
            }
            
            await _context.SaveChangesAsync();
        }

    }
}
