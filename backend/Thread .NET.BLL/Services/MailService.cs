﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using MailKit.Net.Smtp;
using MimeKit;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;
using System.Linq;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class MailService : BaseService
    {
        private MailboxAddress _from;
        private MimeMessage _message;
        private SmtpClient _client;
        private BodyBuilder _bodyBuilder;
        public MailService(ThreadContext context, IMapper mapper) : base(context, mapper) {
            _from = new MailboxAddress("Thread .Net", "netcoremailsendinvite@gmail.com");
            _bodyBuilder = new BodyBuilder();
            _client = new SmtpClient();
            _client.Connect("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTls);
            _client.Authenticate("netcoremailsendinvite@gmail.com", "qweR1234");


        }


        public async Task SendLikeNotification(PostDTO post)
        {
            _message = new MimeMessage();
            _message.From.Add(_from);

            _message.To.Add(new MailboxAddress(post.Author.UserName, post.Author.Email));
            _message.Subject = $"{post.Author.UserName}! Someone just Like your post on Thread.Net";

            var userName = post.Reactions.LastOrDefault().User.UserName;
            _bodyBuilder.TextBody = $"{userName} just liked your post: {post.Body}" ;
            _message.Body = _bodyBuilder.ToMessageBody();
            await _client.SendAsync(_message);
            await _client.DisconnectAsync(true);
            _client.Dispose();
        }

    }
}
