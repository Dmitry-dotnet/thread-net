﻿using AutoMapper;
using Bogus.DataSets;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task DeleteComment(int id)
        {
            var commentToDelete = await _context.Comments.Include(c => c.Reactions).FirstOrDefaultAsync(x => x.Id == id);
            if (commentToDelete == null)
            {
                throw new NullReferenceException();
            }
                _context.Comments.Remove(commentToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<CommentDTO> UpdateComment(CommentDTO commentDTO)
        {
            var commentEntity = _mapper.Map<Comment>(commentDTO);
            if (commentEntity == null)
            {
                throw new NullReferenceException();
            }
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentEntity.Id);
            comment.Body = commentEntity.Body;
            comment.UpdatedAt = DateTime.Now;

            _context.Comments.Update(comment);
            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(comment);
        }
    }
}
