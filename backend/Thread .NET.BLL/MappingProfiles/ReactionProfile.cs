﻿using AutoMapper;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Entities;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.BLL.MappingProfiles
{
    public sealed class ReactionProfile : Profile
    {
        public ReactionProfile()
        {
            CreateMap<Reaction, ReactionDTO>();
            CreateMap<ReactionDTO, Reaction>();

            CreateMap<NewReactionDTO, PostReaction>().ForMember(x => x.PostId, src=> src.MapFrom(a => a.EntityId));
            CreateMap<NewReactionDTO, CommentReaction>().ForMember(x => x.CommentId, src=> src.MapFrom(a => a.EntityId));
            //CreateMap<ReactionDTO, CommentReaction>();

        }
    }
}
