import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { ChangePassword } from 'src/app/models/changePassword/changePassword';
import { map, takeUntil } from 'rxjs/operators';
import { ChangePasswordService } from 'src/app/services/change-password.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent implements OnInit {
  public currenntUser: User;
  public oldPassword: string;
  public newPassword: string;
  public confirmPassword: string;

  constructor(private dialogRef: MatDialogRef<PasswordChangeComponent>,
    private changePasswordService: ChangePasswordService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.currenntUser = data.user
  }

  ngOnInit(): void {
  }

  public changePass() {
    console.log(this.currenntUser);
    let changePass = new ChangePassword(this.currenntUser.id,
      this.oldPassword, this.newPassword);

    this.changePasswordService.changePassword(changePass)
      .subscribe(() => {

        console.log("password changed");
        this.dialogRef.close(false)
      },
        (err) => alert("error"));


  }

  public close() {
    this.newPassword = undefined;
    this.confirmPassword = undefined;
    this.dialogRef.close(false);
  }
}
