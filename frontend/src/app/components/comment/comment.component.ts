import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Post } from 'src/app/models/post/post';
import { LikeService } from 'src/app/services/like.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() public deleting: EventEmitter<Comment> = new EventEmitter<Comment>();
    @Output() public editing: EventEmitter<Comment> = new EventEmitter<Comment>();

    private unsubscribe$ = new Subject<void>();

    public constructor
        (private authService: AuthenticationService,
            private likeService: LikeService) {
        if (!this.currentUser) {
            this.authService.getUser()
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                    }
                })
        }
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public isDeleteButtonVisible(): boolean {
        if (this.currentUser == null) {
            console.log("user is null")
            return false;
        }
        if (this.comment.author.id == null) {
            console.log("author id is null");
            return false;
        }
        return this.comment.author.id === this.currentUser.id;
    }

    public switchEditMode() {
        if (this.comment.isEditing) {

            this.comment.isEditing = false;
        } else {

            this.comment.isEditing = true;
        }

    }

    public sendReaction(islike: boolean) {
        this.likeService
            .likePost(this.comment, this.currentUser, islike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));

        this.comment.reactions = this.comment.reactions.slice();
    }

    public likeCounter(Like: boolean): number {
        if (Like) {
            var count = this.comment.reactions.filter(l => l.isLike == true).length;
        }
        else {
            var count = this.comment.reactions.filter(l => l.isLike == false).length;
        }

        if (count > 0) {
            return count;
        }
        else {
            return null;
        }
    }

    public editComment() {
        this.editing.emit(this.comment);
    }

    public deleteComment() {
        this.deleting.emit(this.comment);
    }

}
