import { Component, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { relative } from 'path';
import { Reaction } from 'src/app/models/reactions/reaction';


@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() public deleting: EventEmitter<Post> = new EventEmitter<Post>();
    @Output() public editing: EventEmitter<Post> = new EventEmitter<Post>();
    @Output() public withImage: EventEmitter<Post> = new EventEmitter<Post>();
    public showComments = false;
    public imageUrl: string;
    public imageFile: File;
    public newComment = {} as NewComment;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postSetvice: PostService,
        private imgurService: ImgurService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public isPrivateButtonVisible(): boolean {
        if (this.currentUser == null) {
            console.log("user is null")
            return false;
        }
        if (this.post.author.id == null) {
            console.log("author id is null");
            return false;
        }
        return this.post.author.id === this.currentUser.id;
    }

    public deletePost() {

        this.deleting.emit(this.post);
    }

    public editPost() {
        this.editing.emit(this.post);
    }

    public imageLoad(target: any) {

        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);

        this.imgurService.uploadToImgur(this.imageFile, 'title').subscribe(
            (res) => {
                this.post.previewImage = res.body.data.link;
                console.log(this.post.previewImage)
            }
        )


    }

    public switchEditMode() {
        if (this.post.isEditing) {

            this.post.isEditing = false;
        } else {

            this.post.isEditing = true;
        }

    }

    public sendReaction(islike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, islike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    console.log(post);
                    this.post = post
                });
            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, islike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post
            });


    }

    public sendNotification() {
        const check = this.post.reactions.filter(x => x.user.userName == this.currentUser.userName)
        if (check.length != 0) {
            this.postSetvice.notify(this.post)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(() => (this.post))
        }
    }

    public likeCounter(Like: boolean): number {
        if (Like) {
            var count = this.post.reactions.filter(l => l.isLike == true).length;
        }
        else {
            var count = this.post.reactions.filter(l => l.isLike == false).length;
        }
        if (count > 0) {
            return count;
        }
        else {
            return null;
        }
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public onCommentDeleting(comment: Comment) {
        this.commentService.deleteComment(comment.id).subscribe(
            (resp) => {
                const index = this.post.comments.indexOf(comment);
                this.post.comments.splice(index, 1);
            }
        )
    }

    public onCommentEditing(comment: Comment) {
        this.commentService.updateComment(comment).subscribe(
            (resp) => {
                this.sortCommentArray(this.post.comments);
                comment.isEditing = false;
            }
        )
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
