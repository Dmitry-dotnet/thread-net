import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public getPost(id: number) {
        return this.httpService.getFullRequest<Post>(`${this.routePrefix}/get/${id}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public deletePost(id: number): Observable<HttpResponse<number>> {
        return this.httpService.deleteFullRequest<number>(`${this.routePrefix}/delete/${id}`);
    }

    public editPost(post: Post): Observable<Post> {
        return this.httpService.putRequest<Post>(`${this.routePrefix}/update`, post);
    }

    public notify(post: Post): Observable<HttpResponse<Post>> {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/notify`, post);
    }
}
