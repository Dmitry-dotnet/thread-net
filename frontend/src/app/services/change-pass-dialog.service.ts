import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PasswordChangeComponent } from '../components/password-change/password-change.component';
import { Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ChangePassDialogService {
  private unsubscribe$ = new Subject<void>();

  constructor(private dialog: MatDialog) { }

  public openChangePassDialog(user: User) {
    const dialog = this.dialog.open(PasswordChangeComponent, {
      data: { user },
      minWidth: 300,
      autoFocus: true,
      backdropClass: 'dialog-backdrop',
      position: {
        top: '0'
      }
    });
    // dialog
    //     .afterClosed()
    //     .pipe(takeUntil(this.unsubscribe$))
    //     .subscribe((result: User) => {
    //         if (result) {
    //             this.authService.setUser(result);
    //         }
    //     });
  }

}
