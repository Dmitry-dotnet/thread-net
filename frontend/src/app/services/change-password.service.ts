import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { ChangePassword } from '../models/changePassword/changePassword';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  constructor(private httpService: HttpInternalService) { }

  public changePassword(changePass: ChangePassword) {
    console.log(changePass);
    return this.httpService.postFullRequest<ChangePassword>('/api/password/', changePass);
  }
}
