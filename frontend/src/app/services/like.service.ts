import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { typeofExpr } from '@angular/compiler/src/output/output_ast';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(
        private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService) { }

    public likePost(post, currentUser: User, isLike: boolean) {
        const innerPost = post;
        const newReaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };
        const reaction: Reaction = {
            isLike: false,
            user: currentUser
        }

        if (isLike) {
            newReaction.isLike = true;
            reaction.isLike = true;
        }
        else {
            newReaction.isLike = false;
            reaction.isLike = false;

        }

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerPost.reactions.concat(reaction);
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);


        //Check if post, because only post has comments
        if (post.comments !== undefined) {
            return this.postService.likePost(newReaction).pipe(
                map(() => innerPost),
                catchError(() => {
                    // revert current array changes in case of any error
                    innerPost.reactions = hasReaction
                        ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                        : innerPost.reactions.concat(reaction);

                    return of(innerPost);
                })
            );
        }

        else {
            return this.commentService.likeComment(newReaction).pipe(
                map(() => innerPost),
                catchError(() => {
                    innerPost.reactions = hasReaction
                        ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                        : innerPost.reactions.concat(reaction);

                    return of(innerPost);
                })
            );
        }

    }
}
