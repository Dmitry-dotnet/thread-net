export class ChangePassword {
    userId: number;
    oldPassword: string;
    newPassword: string;

    constructor(userId: number, oldPassword: string, newPassword: string) {
        this.userId = userId,
            this.oldPassword = oldPassword,
            this.newPassword = newPassword
    }
}