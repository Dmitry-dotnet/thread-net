import { User } from '../user';
import { Reaction } from '../reactions/reaction';
import { Post } from '../post/post';
import { NumberValueAccessor } from '@angular/forms';

export class Comment {
    id: number;
    createdAt: Date;
    author: User;
    body: string;
    reactions: Reaction[];
    isEditing: boolean;

    public constructor() { }
}
